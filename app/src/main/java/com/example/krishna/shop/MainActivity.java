package com.example.krishna.shop;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    Button go;
    Spinner domain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner spinner = (Spinner) findViewById(R.id.spinner_d);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.character, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        go=(Button)findViewById(R.id.btn_go);
        domain=(Spinner)findViewById(R.id.spinner_d);
        spinner.setAdapter(adapter);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             String tdomain;
                tdomain=domain.getSelectedItem().toString();
                if(tdomain.equals("Shopkeeper")){
                    startActivity(new Intent(MainActivity.this,ShopkeeperLogin.class));
                }
                else{
                    startActivity(new Intent(MainActivity.this,CustomerLogin.class));
                }
            }
        });

    }
}
